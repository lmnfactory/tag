<?php

namespace Lmn\Tag;

use Lmn\Core\Provider\LmnServiceProvider;
use Lmn\Core\Lib\Module\ModuleServiceProvider;
use Illuminate\Support\Facades\Route;
use Lmn\Core\Lib\Database\Seeder\SeederService;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;

use Lmn\Account\Middleware\SigninRequiredMiddleware;

use Lmn\Tag\Repository\TagRepository;
use Lmn\Tag\Repository\Criteria\Tag\TagSearchCriteria;
use Lmn\Tag\Repository\Criteria\Tag\TagByValuesCriteria;
use Lmn\Tag\Lib\Tag\TagService;
use Lmn\Tag\Database\Seed\TagSeeder;

use Lmn\Core\Lib\Search\SearchService;
use Lmn\Tag\Lib\Search\TagSearch;

class ModuleProvider implements ModuleServiceProvider{

    public function register(LmnServiceProvider $provider) {
        $app = $provider->getApp();
        // Entry service
        $app->singleton(TagService::class, TagService::class);

        $app->singleton(TagRepository::class, TagRepository::class);

        $app->bind(TagSearch::class, TagSearch::class);

        $this->registerCommands($provider);
    }

    public function boot(LmnServiceProvider $provider) {
        $app = $provider->getApp();

        $seederService = $app->make(SeederService::class);
        $seederService->addSeeder(TagSeeder::class);

        /** @var CriteriaService $criteriaService */
        $criteriaService = $app->make(CriteriaService::class);
        $criteriaService->add('tag.by.values', TagByValuesCriteria::class);
        $criteriaService->add('tag.search', TagSearchCriteria::class);

        $searchService = $app->make(SearchService::class);
        $searchService->add($app->make(TagSearch::class));
    }

    public function event(LmnServiceProvider $provider) {

    }

    public function route(LmnServiceProvider $provider) {
        Route::group(['namespace' => 'Lmn\\Tag\\Controller'], function() {
            Route::any('api/tag/search','TagSearchController@search');
        });
    }

    public function registerCommands($provider){

    }
}
