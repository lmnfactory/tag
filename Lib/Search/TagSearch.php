<?php

namespace Lmn\Tag\Lib\Search;

use Lmn\Core\Lib\Search\Searchable;
use Lmn\Core\Lib\Search\SearchFactory;

use Lmn\Tag\Lib\Tag\TagService;
use Lmn\Tag\Repository\TagRepository;

class TagSearch implements Searchable {

    private $tagRepo;
    private $searchFactory;
    private $tagService;

    public function __construct(TagRepository $tagRepo, SearchFactory $searchFactory, TagService $tagService) {
        $this->tagRepo = $tagRepo;
        $this->searchFactory = $searchFactory;
        $this->tagService = $tagService;
    }

    public function search($search) {
        $tagsQuery = $this->tagRepo->clear()
            ->criteria('tag.search', [
                'limit' => 5,
                'search' => $this->tagService->format($search)
            ]);
            
        $tags = $tagsQuery->all();

        $results = [];
        foreach ($tags as $t) {
            $results[] = $this->searchFactory->result($t, 1, 'tag');
        }

        return $results;
    }
}