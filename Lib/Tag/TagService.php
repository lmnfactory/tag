<?php

namespace Lmn\Tag\Lib\Tag;
use Lmn\Tag\Repository\TagRepository;

class TagService {

    const DASHED = 0;
    const CAMMEL_CASE = 1;
    const PASCAL_CASE = 2;
    const UNDERSCORED = 3;
    const DOTTED = 4;

    private $systemTags;

    private $allowChars;
    private $allowAlpha;
    private $allowNum;

    private $tagRepo;

    public function __construct(TagRepository $tagRepo) {
        $this->tagRepo = $tagRepo;
        $this->allowChars = ['-', '_'];
        $this->allowAlpha = true;
        $this->allowNum = true;
        $this->systemTags = [
            'file' => 1
        ];
    }

    private function checkAllowAlpha($char) {
        return ($this->allowAlpha && ctype_alpha($char));
    }

    private function checkAllowNum($char) {
        return ($this->allowNum && ctype_digit($char));
    }

    private function checkAllowChars($char) {
        return (in_array($char, $this->allowChars));
    }

    private function checkAllowChar($char) {
        return ($this->checkAllowAlpha($char) || $this->checkAllowNum($char) || $this->checkAllowChars($char));
    }

    private function formatAllow($tag) {
        $result = "";
        for ($i = 0; $i < strlen($tag); $i++) {
            if ($this->checkAllowChar($tag[$i])) {
                $result .= $tag[$i];
            }
        }
        return $result;
    }

    public function formatDashed($tag) {
        $result = "";
        $prev = "";
        for ($i = 0; $i < strlen($tag); $i++) {
            if (ctype_upper($tag[$i])) {
                if ($prev != "" && ($this->checkAllowNum($prev) || $this->checkAllowAlpha($prev))) {
                    $result .= "-";
                }
                $tag[$i] = strtolower($tag[$i]);
            }
            else if ($this->checkAllowChars($tag[$i])) {
                $tag[$i] = '-';
            }
            $result .= $tag[$i];
            $prev = $tag[$i];
        }
        return $result;
    }

    public function format($tag, $format = self::DASHED) {
        $tag = $this->formatAllow($tag);
        if ($tag == "") {
            return "";
        }
        return $this->formatDashed($tag);
    }

    public function extractTags($text, $format = self::DASHED) {
        $startTag = false;
        $tag = "";
        $rawTags = [];
        for ($i = 0; $i < strlen($text); $i++) {
            if ((ctype_space($text[$i]) || $text[$i] == ".") && $startTag) {
                $rawTags[] = $tag;
                $tag = "";
                $startTag = false;
            }

            if ($startTag) {
                $tag .= $text[$i];
            }

            if ($text[$i] == "#") {
                $startTag = true;
            }
        }

        if ($tag != "") {
            $rawTags[] = $tag;
            $tag = "";
        }

        $tags = [];
        foreach ($rawTags as $tag) {
            $tag = $this->format($tag);
            if ($tag == "") {
                continue;
            }
            $tags[] = $tag;
        }

        $tags = array_unique($tags);

        return $tags;
    }

    public function getIds($tags) {
        $ids = [];
        $dbTags = $this->tagRepo->clear()
            ->criteria('tag.by.values', ['values' => $tags])
            ->all();

        foreach ($tags as $tag) {
            $assigned = false;
            foreach ($dbTags as $entity) {
                if ($entity->value == $tag) {
                    $ids[] = $entity->id;
                    $assigned = true;
                    break;
                }
            }
            if ($assigned) {
                continue;
            }

            $entity = $this->tagRepo->clear()
                ->create([
                    'value' => $tag,
                    'slug' => $tag
                ]);
            $ids[] = $entity->id;
        }

        return $ids;
    }

    public function getSystemTag($name) {
        if (!isset($this->systemTags[$name])) {
            return 0;
        }
        return $this->systemTags[$name];
    }
}
