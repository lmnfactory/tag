<?php

namespace Lmn\Tag\Repository;
use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Database\Save\SaveService;
use Lmn\Thread\Database\Model\Entry;
use Lmn\Tag\Database\Model\Tag;

class TagRepository extends AbstractEloquentRepository {

    private $saveService;

    public function __construct(CriteriaService $criteriaService, SaveService $saveService) {
        parent::__construct($criteriaService);
        $this->saveService = $saveService;
    }

    public function getModel() {
        return Tag::class;
    }

    public function create($data) {
        $model = $this->getModel();
        $entry = new $model();
        $entry->fill($data);

        $this->saveService->begin($entry)
            ->transaction();

        return $entry;
    }
}
