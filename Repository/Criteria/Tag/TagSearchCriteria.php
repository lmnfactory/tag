<?php

namespace Lmn\Tag\Repository\Criteria\Tag;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class TagSearchCriteria implements Criteria {

    private $search;
    private $limit;

    public function __construct() {

    }

    public function set($args) {
        $this->search = $args['search'];
        $this->limit = $args['limit'];
    }

    public function apply(Builder $query) {
        $query->where('tag.value', 'like', $this->search."%")
            ->where('tag.system', '=', false)
            ->limit($this->limit);
    }
}
