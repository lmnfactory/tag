<?php

namespace Lmn\Tag\Repository\Criteria\Tag;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class TagByValuesCriteria implements Criteria {

    private $values;

    public function __construct() {

    }

    public function set($args) {
        $this->values = $args['values'];
    }

    public function apply(Builder $query) {
        $query->whereIn('tag.value', $this->values);
    }
}
