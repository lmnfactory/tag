<?php

namespace Lmn\Tag\Build\Test;

use TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TagThreadControllerTest extends TestCase {

    use DatabaseTransactions;

    public function testTagSearch() {
        $this->json('POST', '/api/tag/search', [
                'search' => 's'
            ])
            ->assertResponseOk()
            ->seeJsonStructure([
                'data' => [ 0 ]
            ]);
    }
}
