<?php

namespace Lmn\Tag\Database\Seed;

use App;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TagSeeder extends Seeder {

    private function addSystemTags() {
        \DB::table('tag')->insert([
            [
                'id' => 1,
                'value' => 'súbor',
                'slug' => 'subor',
                'system' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);
    }

    private function dev() {
        $this->addSystemTags();
        \DB::table('tag')->insert([
            [
                'value' => 'test',
                'slug' => 'test',
                'system' => 0,
                'created_at' => Carbon::now()->subDays(1)->format('Y-m-d H:i:s')
            ],
            [
                'value' => 'skuska',
                'slug' => 'skuska',
                'system' => 0,
                'created_at' => Carbon::now()->subDays(2)->format('Y-m-d H:i:s')
            ],
            [
                'value' => 'zapocet',
                'slug' => 'zapocet',
                'system' => 0,
                'created_at' => Carbon::now()->subDays(16)->format('Y-m-d H:i:s')
            ]
        ]);
    }

    private function prod() {
        $this->addSystemTags();
        \DB::table('tag')->insert([
            [
                'value' => 'skuska',
                'slug' => 'skuska',
                'system' => 0,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'value' => 'zapocet',
                'slug' => 'zapocet',
                'system' => 0,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);
    }

    public function run() {
        $env = App::environment();
        if ($env == "production") {
            $this->prod();
        } else if ($env == "development") {
            $this->dev();
        }
    }
}
