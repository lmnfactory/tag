<?php

namespace Lmn\Tag\Database\Model;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

    protected $table = 'tag';

    protected $fillable = ['value', 'slug'];
}
