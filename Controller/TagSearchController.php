<?php

namespace Lmn\Tag\Controller;

use App\Http\Controllers\Controller;
use Lmn\Tag\Lib\Tag\TagService;
use Illuminate\Http\Request;
use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Core\Lib\Model\ValidationService;

use Lmn\Tag\Repository\TagRepository;

class TagSearchController extends Controller {

    public function search(Request $request, ResponseService $responseService, TagService $tagService, TagRepository $tagRepo) {
        $data = $request->json()->all();

        if (!isset($data['search']) || $data['search'] == null) {
            $data['search'] = "";
        }

        $data['limit'] = 10;
        $tags = $tagRepo->clear()
            ->criteria('tag.search', [
                'limit' => 10,
                'search' => $tagService->format($data['search'])
            ])
            ->all();

        return $responseService->response($tags);
    }
}
